package com.jctl.template.subscription.service;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import com.jctl.template.subscription.model.Subscription;
import com.jctl.template.subscription.model.SubscriptionState;
import com.jctl.template.subscription.repository.SubscriptionRepository;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import javax.xml.crypto.Data;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class SubscriptionServiceTest {

  @Mock
  SubscriptionRepository subscriptionRepository;

  @InjectMocks
  SubscriptionService subscriptionService;

  @Test()
    public void testGetSubscriptions(){
    List<Subscription> subscriptions = new ArrayList<>();
    subscriptions.add(
        Subscription.builder()
            .subscriptionId(UUID.randomUUID())
            .subscriptionState(SubscriptionState.CREATED)
            .creationTime(new Date())
            .description("Subscription for Orreiley courses")
            .expirationDate(new Date())
            .build()
    );
    when(subscriptionRepository.findAll()).thenReturn(subscriptions);
    assertEquals(subscriptionService.getAllSubscriptions().size(), 1);
  }

  @Test
  public void testSaveSubscriptions(){
    List<Subscription> subscriptions = new ArrayList<>();
    subscriptions.add(
        Subscription.builder().id(1l)
            .subscriptionId(UUID.randomUUID())
            .subscriptionState(SubscriptionState.CREATED)
            .creationTime(new Date())
            .description("Subscription for Orreiley courses")
            .expirationDate(new Date())
            .build());

    when(subscriptionRepository.saveAll(List.of())).thenReturn(subscriptions);
    assertEquals(subscriptionService.saveSubscriptions(List.of()).size(), 1);
  }


}
