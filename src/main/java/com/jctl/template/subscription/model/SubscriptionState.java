package com.jctl.template.subscription.model;

public enum SubscriptionState {
  CREATED,
  DELETED,
  CANCELLED,
  IN_PROGRESS
}
