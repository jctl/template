package com.jctl.template.subscription.model;

import java.util.Date;
import java.util.Objects;
import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name="subscriptions")
@Builder
public class Subscription {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "id", nullable = false)
  private Long id;

  @Column(name = "subscription_id")
  private UUID subscriptionId;

  @Column(name = "creation_time")
  private Date creationTime;

  @Column(name = "expiration_time")
  private Date expirationDate;

  @Column(name = "description")
  private String description;

  @Enumerated(EnumType.ORDINAL)
  @Column(name = "state")
  private SubscriptionState subscriptionState;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof Subscription)) {
      return false;
    }
    Subscription that = (Subscription) o;
    return getSubscriptionId().equals(that.getSubscriptionId()) && getCreationTime().equals(
        that.getCreationTime()) && getExpirationDate().equals(that.getExpirationDate())
        && getDescription().equals(that.getDescription())
        && getSubscriptionState() == that.getSubscriptionState();
  }

  @Override
  public int hashCode() {
    return Objects.hash(getSubscriptionId(), getCreationTime(), getExpirationDate(),
        getDescription(),
        getSubscriptionState());
  }
}
