package com.jctl.template.subscription.controller;

import com.jctl.template.subscription.model.Subscription;
import com.jctl.template.subscription.service.SubscriptionService;
import java.util.List;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/subscription")
@AllArgsConstructor

public class SubscriptionController {

  private final SubscriptionService subscriptionService;

  @GetMapping
  public ResponseEntity<?> getSubscriptions(){
    List response = subscriptionService.getAllSubscriptions();
    if (response.isEmpty()){
      return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
    return ResponseEntity.ok(response);
  }

  @PostMapping
  public ResponseEntity<?> saveSubscriptions(@RequestBody List<Subscription> subscriptions){
    subscriptionService.saveSubscriptions(subscriptions);
    List response = subscriptionService.saveSubscriptions(subscriptions);
    return new ResponseEntity(response, HttpStatus.CREATED);
  }

  @DeleteMapping
  public ResponseEntity<?> deleteSubscription(@RequestBody Subscription subscription){
    return new ResponseEntity( subscriptionService.deleteSubscription(subscription), HttpStatus.OK);
  }

  @PutMapping
  public ResponseEntity<?> updateSubscription(@RequestBody Subscription subscription){
    return new ResponseEntity( subscriptionService.updateSubscription(subscription), HttpStatus.OK);
  }





}
