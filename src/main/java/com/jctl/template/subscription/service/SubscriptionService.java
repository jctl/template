package com.jctl.template.subscription.service;

import com.jctl.template.subscription.model.Subscription;
import com.jctl.template.subscription.repository.SubscriptionRepository;
import java.util.List;
import java.util.UUID;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
@Log4j2
public class SubscriptionService {

  private final SubscriptionRepository subscriptionRepository;

  public List<Subscription> getAllSubscriptions(){
    return subscriptionRepository.findAll();
  }

  public List<Subscription> saveSubscriptions(List<Subscription> subscriptions){
    return subscriptionRepository.saveAll(subscriptions);
  }

  public boolean deleteSubscription(Subscription id){
    boolean response = true;
    try{
      subscriptionRepository.delete(id);
    }catch(Exception ex){
      log.error(ex);
      response=false;
    }
    return response;
  }

  public Subscription updateSubscription(Subscription subscription){
    return subscriptionRepository.save(subscription);
  }

}
